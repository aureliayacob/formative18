package com.belajarmockito.Mahasiswa.Mahasiswa;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class MahasiswaControllerTest {
	private Mahasiswa model = new Mahasiswa();
	private ArrayList<Mahasiswa> kelasA = new ArrayList<Mahasiswa>();

	@Mock
	private DataService dataServiceMock;

	@InjectMocks
	MahasiswaController mc = new MahasiswaController(model);

	@BeforeEach
	public void init() {
		Mahasiswa mhs = new Mahasiswa("175314-024","Aurelia",21);
		kelasA.add(mhs);
		mc.setKelas(kelasA);
	}

	@Test
	void addMahasiswaTest_NormalCase() {
		//	when(dataServiceMock.retrieveData()).thenReturn(new Mahasiswa("175314-024","Reena",21));
		assertTrue(mc.addMahasiswa("175314-024","Aurelia",21));
	}

	@Test
	void addMahasiswaTest_FalseNIMCase() {
		//	when(dataServiceMock.retrieveData()).thenReturn(new Mahasiswa("175314024","Aurelia",21));
		assertFalse(mc.addMahasiswa("175314024","Aurelia",21));
	}

	@Test
	void addMahasiswaTest_FalseNameCase() {
		//	when(dataServiceMock.retrieveData()).thenReturn(new Mahasiswa("175314-024","Popi4j4h",21));
		assertFalse(mc.addMahasiswa("175314-024","Popi4j4h",21));

	}
	@Test
	void addMahasiswaTest_FalseUmurCase() {
		//	when(dataServiceMock.retrieveData()).thenReturn(new Mahasiswa("175314-024","Ayuna",11));
		assertFalse(mc.addMahasiswa("175314-024","Ayuna",11));
	}


	@Test
	void deleteMahasiswa_NormalCase() {
		when(dataServiceMock.retrieveNim()).thenReturn("175314-024");
		assertTrue(mc.deleteMahasiswa());

	}

	@Test
	void deleteMahasiswa_NotFoundCase() {
		when(dataServiceMock.retrieveNim()).thenReturn("175314-025");
		assertFalse(mc.deleteMahasiswa());

	}

	@Test
	void deleteMahasiswa_FalseNimCase() {
		when(dataServiceMock.retrieveNim()).thenReturn("175314024");
		assertFalse(mc.deleteMahasiswa());

	}

	@Test
	void getDataMahasiswa_NormalCase() {
		when(dataServiceMock.retrieveNim()).thenReturn("175314-024");
		assertEquals("175314-024",mc.getDataMahasiswa().getNim());
	}

	@Test
	void getDataMahasiswa_NotFoundCase() {
		when(dataServiceMock.retrieveNim()).thenReturn("175314-025");
		assertNull(mc.getDataMahasiswa());
	}

	@Test
	void getDataMahasiswa_InvalidCase() {
		when(dataServiceMock.retrieveNim()).thenReturn("175314024");
		assertNull(mc.getDataMahasiswa());
	}

	
}
