package com.belajarmockito.Mahasiswa.Mahasiswa;

public interface DataService {
	Mahasiswa retrieveData();
	String retrieveNim();
}
