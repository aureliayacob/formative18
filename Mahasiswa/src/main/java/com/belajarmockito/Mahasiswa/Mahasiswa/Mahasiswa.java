package com.belajarmockito.Mahasiswa.Mahasiswa;

public class Mahasiswa {
	private String nim;
	private String nama;
	private int umur;
	public Mahasiswa() {

	}

	public Mahasiswa(String nim, String nama, int umur) {
		super();
		this.nim = nim;
		this.nama = nama;
		this.umur = umur;
	}

	public String getNim() {
		return nim;
	}

	public void setNim(String nim) {
		this.nim = nim;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public int getUmur() {
		return umur;
	}

	public void setUmur(int umur) {
		this.umur = umur;
	}



}
