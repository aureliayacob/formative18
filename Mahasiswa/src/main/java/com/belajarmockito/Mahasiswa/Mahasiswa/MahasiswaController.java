package com.belajarmockito.Mahasiswa.Mahasiswa;

import java.util.*;
import java.util.regex.*;

public class MahasiswaController {
	private DataService dataService;
	private Mahasiswa model;
	private ArrayList<Mahasiswa> kelas = new ArrayList<Mahasiswa>();

	public MahasiswaController(Mahasiswa model) {
		super();
		this.model = model;
		this.dataService = dataService;
	}

	public boolean addMahasiswa(String nim, String nama, int umur ) {
		//Mahasiswa data = dataService.retrieveData();
		boolean nameValid = verifyName(nama);
		boolean nimValid = verifyNIM(nim);
		boolean umurValid = verifyUmur(umur);
		if(nameValid&&nimValid&&umurValid) {
			model.setNama(nama);
			model.setNim(nim);
			model.setUmur(umur);
			return kelas.add(model);
		}
		return false;
	}

	public boolean deleteMahasiswa() {
		String nim = dataService.retrieveNim();
		boolean nimValid = verifyNIM(nim);
		if(nimValid) {
			for(Mahasiswa mhs : kelas) {
				if(mhs.getNim().equals(nim)) {
					mhs.setNim("");
					mhs.setNama("");
					mhs.setUmur(0);
					return true;
				}else {
					System.out.println("not found");
					return false;	
				}
			}
		}
		System.out.println("nim invalid");
		return false;
	}

	public Mahasiswa getDataMahasiswa() {
		String nim = dataService.retrieveNim();
		boolean nimValid = verifyNIM(nim);
		if(nimValid) {
			for(Mahasiswa mhs : kelas) {
				if(mhs.getNim().equals(nim)) {
					return mhs;
				}else {
					System.out.println("not found");
					return null;	
				}
			}
		}
		System.out.println("nim invalid");
		return null;
	}
	public boolean verifyNIM(String nim) {
		if(Pattern.compile("^175314-").matcher(nim).find()) {
			model.setNim(nim);
			return true;
		}
		return false;
	}

	public boolean verifyName(String name) {
		if(Pattern.compile("[A-z]{4,45}").matcher(name).matches()) {
			model.setNama(name);
			return true;
		}
		return false;
	}

	public boolean verifyUmur(int umur) {
		String umurStr = Integer.toString(umur);
		if(Pattern.compile("1[8-9]|[2-9]\\d").matcher(umurStr).matches()) {
			model.setUmur(umur);
			return true;
		}
		return false;
	}



	public void setKelas(ArrayList<Mahasiswa> kelas) {
		this.kelas = kelas;
	}
}
